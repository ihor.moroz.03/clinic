﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    internal class CrudCollection<T> : ICrudCollection<T> where T : class
    {
        readonly ICollection<T> _collection;

        public CrudCollection(ICollection<T> values)
        {
            _collection = values;
        }

        public event EventHandler<T>? OnAdd;
        public event EventHandler<T>? OnRemove;
        public event EventHandler<T>? OnUpdate;

        public void Add(T item)
        {
            _collection.Add(item);
            OnAdd?.Invoke(this, item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        public void Remove(T item)
        {
            _collection.Remove(item);
            OnRemove?.Invoke(this, item);
        }

        public void Update(T item)
        {
            OnUpdate?.Invoke(this, item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }
    }
}
