﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic.DAL
{
    public class TreatmentEntry
    {
        public int Id { get; set; }

        public DateTime Time { get; set; }

        public Patient Patient { get; set; }

        public string Description { get; set; }
    }
}
