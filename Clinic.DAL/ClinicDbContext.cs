﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic.DAL
{
    public class ClinicDbContext : DbContext
    {
        public virtual DbSet<TreatmentEntry> Treatments { get; set; }

        public virtual DbSet<Patient> Patients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseSqlServer(
                "Server=DESKTOP-J41D9GD\\SQLEXPRESS;Database=Clinic;TrustServerCertificate=True;Integrated Security=True;"
            );

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Patient>()
                .HasData(
                    new Patient()
                    {
                        Id = 1,
                        FirstName = "Sara",
                        LastName = "Jones",
                        BirthDate = new DateTime(1990, 1, 1)
                    },
                    new Patient()
                    {
                        Id = 2,
                        FirstName = "Wednesday",
                        LastName = "Addams",
                        BirthDate = new DateTime(1990, 1, 1)
                    },
                    new Patient()
                    {
                        Id = 3,
                        FirstName = "John",
                        LastName = "Jones",
                        BirthDate = new DateTime(1990, 1, 1)
                    }
                );
            modelBuilder
                .Entity<TreatmentEntry>()
                .HasData(
                    new
                    {
                        Id = 1,
                        PatientId = 3,
                        Time = new DateTime(2022, 12, 9),
                        Description = "Prescribed new drug",
                    }
                );

            modelBuilder.Entity<TreatmentEntry>().Navigation(t => t.Patient).AutoInclude();
        }
    }
}
