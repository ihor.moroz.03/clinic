﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Clinic.DAL;

namespace Clinic.WebApi.Controllers
{
    [ApiController]
    [Route("treatments")]
    public class BookEntitiesController : ControllerBase
    {
        private readonly ClinicDbContext _context;

        public BookEntitiesController(ClinicDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TreatmentEntry>>> GetTreatments()
        {
            return await _context.Treatments.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TreatmentEntry>> GetTreatment(long id)
        {
            var treatment = await _context.Treatments.FindAsync(id);

            if (treatment == null)
            {
                return NotFound();
            }

            return treatment;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTreatment(long id, TreatmentEntry treatment)
        {
            if (id != treatment.Id)
            {
                return BadRequest();
            }

            _context.Entry(treatment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TreatmentEntryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<TreatmentEntry>> PostTreatment(TreatmentEntry treatment)
        {
            _context.Treatments.Add(treatment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBookEntity", new { id = treatment.Id }, treatment);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTreatment(long id)
        {
            var bookEntity = await _context.Treatments.FindAsync(id);
            if (bookEntity == null)
            {
                return NotFound();
            }

            _context.Treatments.Remove(bookEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TreatmentEntryExists(long id)
        {
            return _context.Treatments.Any(e => e.Id == id);
        }
    }
}
